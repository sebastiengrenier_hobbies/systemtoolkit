/*
 * LM358CurrentSensor.cpp
 *
 *  Created on: 27 mars 2021
 *      Author: Sébastien GRENIER
 */

#include "LM358CurrentSensor.h"

LM358CurrentSensor::LM358CurrentSensor(uint8_t anAnalogPinNumber,
		uint16_t aVccMilli) :
		CurrentSensor(anAnalogPinNumber)
{
	vccMilli = aVccMilli;
}

bool LM358CurrentSensor::convertSample(uint16_t sample, uint16_t *output)
{
	if ((sample == 0) || (sample > 1023))
		return false;

	long value = (uint32_t) sample;
	value *= vccMilli;
	value *= 2; // LM358 give us 500 mV for 1000 mA
	value /= (uint32_t) 1023;
	if (value < 0)
		value = 0;

	*output = (uint16_t) value;

	return true;
}
