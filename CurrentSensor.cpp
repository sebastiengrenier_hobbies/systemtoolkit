/*
 * CurrentSensor.cpp
 *
 *  Created on: 3 mars 2021
 *      Author: Sébastien GRENIER
 */

#include "CurrentSensor.h"

//==============================================================================
//
// Construction / destruction
//
//==============================================================================

CurrentSensor::CurrentSensor(uint8_t anAnalogPinNumber)
{
	analogPinNumber = anAnalogPinNumber;
	started = false;
}

//------------------------------------------------------------------------------

CurrentSensor::~CurrentSensor()
{
}

//==============================================================================
//
// Public operations
//
//==============================================================================

void CurrentSensor::start()
{
	if (started)
		return;

	pinMode(analogPinNumber, INPUT);
	started = true;
}

//------------------------------------------------------------------------------

void CurrentSensor::stop()
{
	if (!started)
		return;

	started = false;
}

//------------------------------------------------------------------------------

uint16_t CurrentSensor::senseAverageSample(uint16_t sampleCount)
{
	uint32_t sum = 0;
	uint16_t count = 0;
	uint16_t converted;

	while (sampleCount--)
	{
		int sample = analogRead(analogPinNumber);

		if (sample < 0)
			sample = -sample;

		if (convertSample(sample, &converted))
		{
			sum += converted;
			count++;
		}
	}

	if (count == 0)
		return 0;

	return sum / count;
}

//------------------------------------------------------------------------------

uint16_t CurrentSensor::senseDurationSample(uint16_t milliseconds)
{
	uint32_t end = millis() + milliseconds;

	uint32_t sum = 0;
	uint16_t count = 0;
	uint16_t converted;

	while (millis() < end)
	{
		int sample = analogRead(analogPinNumber);

		if (sample < 0)
			sample = -sample;

		if (convertSample(sample, &converted))
		{
			sum += converted;
			count++;
		}
	}

	if (count == 0)
		return 0;

	return sum / count;
}

//------------------------------------------------------------------------------

uint16_t CurrentSensor::sensePeakDuration(uint16_t milliseconds,
		uint16_t peakCurrent)
{
	uint32_t end = millis() + milliseconds;
	uint32_t first = 0;
	uint32_t last = 0;

	uint32_t sum = 0;
	uint16_t count = 0;
	uint16_t converted;

	uint32_t timestamp = millis();
	while (timestamp < end)
	{
		int sample = analogRead(analogPinNumber);

		if (sample < 0)
			sample = -sample;

		if (convertSample(sample, &converted))
		{
			sum += converted;
			count++;

			if (converted > peakCurrent)
			{
				if (first == 0)
					first = timestamp;
				last = timestamp;
			}
		}

		timestamp = millis();
	}

	if (count == 0)
		return 0;

	if (sum / count >= peakCurrent)
		return last - first;

	return 0;
}

//==============================================================================
//
// Internal operations
//
//==============================================================================

