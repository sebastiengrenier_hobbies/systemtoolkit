## **SystemToolkit Library V1.0** for Arduino

**Written by:** _Sébastien GRENIER_  

### **What is the SystemToolkit library**

Brief about what this library is...

### **How to use it**

Some lines to show how to use the library...

```Arduino
#ifndef SYSTEMTOOLKITCONFIG_H_
#define SYSTEMTOOLKITCONFIG_H_

#define SYSTEM_TOOLKIT_ENABLE_TIMER_0
#define SYSTEM_TOOLKIT_ENABLE_TIMER_1
#define SYSTEM_TOOLKIT_ENABLE_TIMER_2

#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
#define SYSTEM_TOOLKIT_ENABLE_TIMER_3
#define SYSTEM_TOOLKIT_ENABLE_TIMER_4
#define SYSTEM_TOOLKIT_ENABLE_TIMER_5
#endif

#endif

```


```Arduino
#include <SystemToolkit.h>

void setup(){

}

void loop(){

}

```



### **Library functions**

#### **Timer::getInstance( timerNumber )**

This function allows you to read a single byte of data from the eeprom.
Its only parameter is an `int` which should be set to the address you wish to read.

The function returns an `unsigned char` containing the value read.
