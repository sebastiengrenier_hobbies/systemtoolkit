/*
 * DimmingMultiplexer.cpp
 *
 *  Created on: 18 mars 2021
 *      Author: Sébastien GRENIER
 */

#include <DimmingMultiplexer.h>

//------------------------------------------------------------------------------
// Timing for 8 Mhz oscillator clocked ARDUINO
//------------------------------------------------------------------------------

#if (F_CPU==8000000L)

#define DIMMING_SUBDIVIDER	125

//------------------------------------------------------------------------------
// Timing for 16 Mhz standard ARDUINO
//------------------------------------------------------------------------------

#elif (F_CPU==16000000L)

#define DIMMING_SUBDIVIDER	250

//------------------------------------------------------------------------------
// Default to standard ARDUINO timer
//------------------------------------------------------------------------------
#else

#define DIMMING_SUBDIVIDER	250

#endif

DimmingMultiplexer *DimmingMultiplexer::GLOBAL_INSTANCE = NULL;

DimmingMultiplexer* DimmingMultiplexer::initialize(uint8_t aTimerNumber,
		uint8_t aChannelCount)
{
	GLOBAL_INSTANCE = new DimmingMultiplexer(aTimerNumber, aChannelCount);
	return GLOBAL_INSTANCE;
}

DimmingMultiplexer::DimmingMultiplexer(uint8_t aTimerNumber,
		uint8_t aChannelCount)
{
	timer = Timer::getInstance(aTimerNumber);

	tick = 0;
	clockTick = 0;
	tenMilliseconds = 0;

	suspended = false;

	channelCount = aChannelCount;
	levels = new uint8_t[channelCount];
	bits = new uint8_t[channelCount];
	reversed = new bool[channelCount];
	ports = new volatile uint8_t*[channelCount];

	memset(levels, 0, channelCount);
}

DimmingMultiplexer::~DimmingMultiplexer()
{
}

void DimmingMultiplexer::start()
{
	suspended = false;
	tick = 0;
	clockTick = 0;
	tenMilliseconds = 0;

	for (uint8_t index = 0; index < channelCount; index++)
		setChannelOff(index);

	//
	// We start the timer using a prescale of 8 and a value of 125
	// This will divide the 16 Mhz clock by 8*250 = 2000 and provide a 8000 Hz
	// base clock. As we manage 20 levels for each PWM channel, we will produce
	// a 8000 / 20 = 400 Hz pulse of 20 different width which is sufficient in
	// most cases to avoid flicker.
	//
	// We also produce a 100th of second every 80 pulses
	//

	timer->setEventHandler(this);
	timer->enableOutputCompareAInterrupt(true);

	timer->setOutputCompareAMode(COMPARE_MODE_NONE);

	if (timer->getNumber() == 2)
		timer->setClockMode(CLOCK_MODE_TIMER2_INTERNAL_PRESCALE_8);
	else
		timer->setClockMode(CLOCK_MODE_INTERNAL_PRESCALE_8);

	if (timer->getResolution() == 8)
		timer->setTimerMode(TIMER_8BIT_MODE_CLEAR_TIMER_ON_MATCH_OCR);
	else
		timer->setTimerMode(TIMER_16BIT_MODE_CLEAR_TIMER_ON_MATCH_OCR);

	timer->setOutputCompareA(DIMMING_SUBDIVIDER);

	timer->start();
}

void DimmingMultiplexer::onTimerOutputMatchA()
{
	if (suspended)
		return;

	clockTick++;
	if (clockTick >= 80)
	{
		clockTick = 0;
		tenMilliseconds++;
	}

	if (tick >= 20)
		tick = 0;

	if (tick == 0)
	{
		//
		// On reset, switch ON all channels having a level > ZERO
		//

		for (uint8_t index = 0; index < channelCount; index++)
		{
			if (levels[index])
				setChannelOn(index);
			else
				setChannelOff(index);
		}
	}
	else
	{
		//
		// On tick, switch OFF all channels having level == tick
		//

		for (uint8_t index = 0; index < channelCount; index++)
		{
			if (levels[index] == tick)
				setChannelOff(index);
		}
	}

	tick++;
}

void DimmingMultiplexer::suspend()
{
	suspended = true;
}

void DimmingMultiplexer::resume()
{
	suspended = false;
}

void DimmingMultiplexer::registerChannel(uint8_t index, uint8_t pinNumber,
		boolean aReversed)
{
	if (index >= channelCount)
		return;

	bits[index] = digitalPinToBitMask(pinNumber);
	ports[index] = portOutputRegister(digitalPinToPort(pinNumber));
	reversed[index] = aReversed;

	pinMode(pinNumber, OUTPUT);
	*ports[index] &= (~bits[index]);
}

void DimmingMultiplexer::setChannelLevel(uint8_t index,
		uint8_t dimmingLevel0To20)
{
	levels[index] = dimmingLevel0To20;
}

void DimmingMultiplexer::setChannelOn(uint8_t index)
{
	if (reversed[index])
		*ports[index] &= ~bits[index];
	else
		*ports[index] |= bits[index];
}

void DimmingMultiplexer::setChannelOff(uint8_t index)
{
	if (reversed[index])
		*ports[index] |= bits[index];
	else
		*ports[index] &= ~bits[index];
}
