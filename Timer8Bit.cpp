/*
 * Timer8Bit.cpp
 *
 *  Created on: 8 mars 2021
 *      Author: Sébastien GRENIER
 */

#include "Timer8Bit.h"

//==============================================================================
//
// Construction / destruction
//
//==============================================================================

Timer8Bit::Timer8Bit(uint8_t aTimerNumber) :
		Timer(aTimerNumber)
{
	switch (aTimerNumber)
	{
#ifdef SYSTEM_TOOLKIT_ENABLE_TIMER_0
		case 0:

			outputCompareA = &OCR0A;
			outputCompareB = &OCR0B;

			break;
#endif

#ifdef SYSTEM_TOOLKIT_ENABLE_TIMER_2
	case 2:

		outputCompareA = &OCR2A;
		outputCompareB = &OCR2B;

		break;
#endif
	}
}

//------------------------------------------------------------------------------

Timer8Bit::~Timer8Bit()
{
}

//==============================================================================
//
// Public operations
//
//==============================================================================

void Timer8Bit::enableOutputCompareAInterrupt(bool value)
{
	if (value)
		bitSet(*interruptMask, outputCompareAInterruptEnableBit);
	else
		bitClear(*interruptMask, outputCompareAInterruptEnableBit);
}

//------------------------------------------------------------------------------

void Timer8Bit::enableOutputCompareBInterrupt(bool value)
{
	if (value)
		bitSet(*interruptMask, outputCompareBInterruptEnableBit);
	else
		bitClear(*interruptMask, outputCompareBInterruptEnableBit);
}

//------------------------------------------------------------------------------

void Timer8Bit::enableOverflowInterrupt(bool value)
{
	if (value)
		bitSet(*interruptMask, overflowInterruptEnableBit);
	else
		bitClear(*interruptMask, overflowInterruptEnableBit);
}

//------------------------------------------------------------------------------

void Timer8Bit::setOutputCompareAMode(OUTPUT_COMPARE_MODE value)
{
	bitWrite(*controlRegisterA, outputCompareAModeBit0, value & 0x01);
	bitWrite(*controlRegisterA, outputCompareAModeBit1, value & 0x02);
}

//------------------------------------------------------------------------------

void Timer8Bit::setOutputCompareA(uint16_t value)
{
	*outputCompareA = value & 0xFF;
}

//------------------------------------------------------------------------------

void Timer8Bit::setOutputCompareBMode(OUTPUT_COMPARE_MODE value)
{
	bitWrite(*controlRegisterA, outputCompareBModeBit0, value & 0x01);
	bitWrite(*controlRegisterA, outputCompareBModeBit1, value & 0x02);
}

//------------------------------------------------------------------------------

void Timer8Bit::setOutputCompareB(uint16_t value)
{
	*outputCompareB = value & 0xFF;
}

//------------------------------------------------------------------------------

void Timer8Bit::setClockMode(CLOCK_MODE clockMode)
{
	bitWrite(*controlRegisterB, clockSelectBit0, clockMode & 0x01);
	bitWrite(*controlRegisterB, clockSelectBit1, clockMode & 0x02);
	bitWrite(*controlRegisterB, clockSelectBit2, clockMode & 0x04);
}

//------------------------------------------------------------------------------

void Timer8Bit::setTimerMode(TIMER_MODE operationMode)
{
	bitWrite(*controlRegisterA, waveGeneratorModeBit0, operationMode & 0x01);
	bitWrite(*controlRegisterA, waveGeneratorModeBit1, operationMode & 0x02);
	bitWrite(*controlRegisterB, waveGeneratorModeBit2, operationMode & 0x04);
}
