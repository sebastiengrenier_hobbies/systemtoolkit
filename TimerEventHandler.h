/*
 * TimerEventHandler.h
 *
 *  Created on: 5 mars 2021
 *      Author: Sébastien GRENIER
 */

#ifndef TIMEREVENTHANDLER_H_
#define TIMEREVENTHANDLER_H_

class TimerEventHandler {
public:
	TimerEventHandler();
	virtual ~TimerEventHandler();

	virtual void onTimerOverflow() {
	}
	virtual void onTimerOutputMatchA() {
	}
	virtual void onTimerOutputMatchB() {
	}
	virtual void onTimerOutputMatchC() {
	}
	virtual void onTimerInputCapture() {
	}
};

#endif /* TIMEREVENTHANDLER_H_ */
