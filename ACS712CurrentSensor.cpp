/*
 * ACS712CurrentSensor.cpp
 *
 *  Created on: 3 mars 2021
 *      Author: Sébastien GRENIER
 */

#include "ACS712CurrentSensor.h"

ACS712CurrentSensor::ACS712CurrentSensor(uint8_t anAnalogPinNumber,
		uint16_t aVccMilli, uint16_t aMilliVoltsPerAmps) :
		CurrentSensor(anAnalogPinNumber)
{
	vccMilli = aVccMilli;
	milliVoltsPerAmps = aMilliVoltsPerAmps;
}

bool ACS712CurrentSensor::convertSample(uint16_t sample, uint16_t *output)
{
	//
	// Convert a single 1023/2 relative sample to its absolute value
	//

	uint32_t adjust;

	if (sample < 512)
		adjust = 512 - sample;
	else
		adjust = sample - 511;

	//
	// Now apply sensor scale to obtain mA reading. As always, we do multiply
	// first then divide, so that we can keep the whole precision and avoid
	// floating point roundings
	//
	// 1) milliVolts = adjust * vccMilli / 1023
	// 2) Amps = milliVolts / milliVoltsPerAmp
	// 2bis) so milliAmps = 1000 * millivolts / milliVoltsPerAmp
	//

	adjust *= vccMilli;
	adjust *= 1000;
	adjust /= milliVoltsPerAmps;
	adjust /= 1023;

	*output = (uint16_t) adjust;

	return true;
}

