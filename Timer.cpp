/*
 * Timer.cpp
 *
 *  Created on: 5 mars 2021
 *      Author: Sébastien GRENIER
 */

#include "Timer.h"

#include "Timer8Bit.h"
#include "Timer16Bit.h"

//#include "SystemToolkitConfig.h"

//==============================================================================
//
// Context instances
//
//==============================================================================

#ifdef SYSTEM_TOOLKIT_ENABLE_TIMER_0
INTERRUPT_CONTEXT INTERRUPT_CONTEXT_0;
#endif

#ifdef SYSTEM_TOOLKIT_ENABLE_TIMER_1
INTERRUPT_CONTEXT INTERRUPT_CONTEXT_1;
#endif

#ifdef SYSTEM_TOOLKIT_ENABLE_TIMER_2
INTERRUPT_CONTEXT INTERRUPT_CONTEXT_2;
#endif

#ifdef SYSTEM_TOOLKIT_ENABLE_TIMER_3
INTERRUPT_CONTEXT INTERRUPT_CONTEXT_3;
#endif

#ifdef SYSTEM_TOOLKIT_ENABLE_TIMER_4
INTERRUPT_CONTEXT INTERRUPT_CONTEXT_4;
#endif

#ifdef SYSTEM_TOOLKIT_ENABLE_TIMER_5
INTERRUPT_CONTEXT INTERRUPT_CONTEXT_5;
#endif

//==============================================================================
//
// Factory
//
//==============================================================================

Timer* Timer::getInstance(uint8_t timerNumber)
{
	switch (timerNumber)
	{
		case 0:
		case 2:
			return new Timer8Bit(timerNumber);

		default:
			return new Timer16Bit(timerNumber);
	}

	return NULL;
}

//==============================================================================
//
// Construction / destruction
//
//==============================================================================

Timer::Timer(uint8_t aTimerNumber)
{
	timerNumber = aTimerNumber;
	started = false;

	switch (timerNumber)
	{

#ifdef SYSTEM_TOOLKIT_ENABLE_TIMER_0
		case 0:
			resolution = 8;

			context = &INTERRUPT_CONTEXT_0;

#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
			powerReductionRegister = &PRR0;
#else
			powerReductionRegister = NULL;
#endif
			powerReductionEnableTimerBit = PRTIM0;

			controlRegisterA = &TCCR0A;
			controlRegisterB = &TCCR0B;
			controlRegisterC = NULL;

			timerCounterLow = &TCNT0;
			timerCounterHigh = NULL;

			interruptMask = &TIMSK0;

			inputCaptureInterruptEnableBit = 0;
			outputCompareAInterruptEnableBit = OCIE0A;
			outputCompareBInterruptEnableBit = OCIE0B;
			outputCompareCInterruptEnableBit = 0;
			overflowInterruptEnableBit = TOIE0;

			interruptFlag = &TIFR0;

			inputCaptureInterruptFlag = 0;
			outputCompareAInterruptFlag = OCF0A;
			outputCompareBInterruptFlag = OCF0B;
			outputCompareCInterruptFlag = 0;
			overflowInterruptFlag = TOV0;

			clockSelectBit0 = CS00;
			clockSelectBit1 = CS01;
			clockSelectBit2 = CS02;

			waveGeneratorModeBit0 = WGM00;
			waveGeneratorModeBit1 = WGM01;
			waveGeneratorModeBit2 = WGM02;
			waveGeneratorModeBit3 = 0;

			outputCompareAModeBit0 = COM0A0;
			outputCompareAModeBit1 = COM0A1;

			outputCompareBModeBit0 = COM0B0;
			outputCompareBModeBit1 = COM0B1;

			outputCompareCModeBit0 = 0;
			outputCompareCModeBit1 = 0;

			break;
#endif

#ifdef SYSTEM_TOOLKIT_ENABLE_TIMER_1
		case 1:

			resolution = 16;
			context = &INTERRUPT_CONTEXT_1;

#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
			powerReductionRegister = &PRR0;
#else
			powerReductionRegister = NULL;
#endif
			powerReductionEnableTimerBit = PRTIM1;

			controlRegisterA = &TCCR1A;
			controlRegisterB = &TCCR1B;
			controlRegisterC = &TCCR1C;

			timerCounterLow = &TCNT1L;
			timerCounterHigh = &TCNT1H;

			interruptMask = &TIMSK1;
			inputCaptureInterruptEnableBit = ICIE1;
			outputCompareAInterruptEnableBit = OCIE1A;
			outputCompareBInterruptEnableBit = OCIE1B;
#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
			outputCompareCInterruptEnableBit = OCIE1C;
#endif
			overflowInterruptEnableBit = TOIE1;

			interruptFlag = &TIFR1;
			inputCaptureInterruptFlag = ICF1;
			outputCompareAInterruptFlag = OCF1A;
			outputCompareBInterruptFlag = OCF1B;
#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
			outputCompareCInterruptFlag = OCF1C;
#endif
			overflowInterruptFlag = TOV1;

			clockSelectBit0 = CS10;
			clockSelectBit1 = CS11;
			clockSelectBit2 = CS12;

			waveGeneratorModeBit0 = WGM10;
			waveGeneratorModeBit1 = WGM11;
			waveGeneratorModeBit2 = WGM12;
			waveGeneratorModeBit3 = WGM13;

			outputCompareAModeBit0 = COM1A0;
			outputCompareAModeBit1 = COM1A1;

			outputCompareBModeBit0 = COM1B0;
			outputCompareBModeBit1 = COM1B1;

#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
			outputCompareCModeBit0 = COM1C0;
			outputCompareCModeBit1 = COM1C1;
#endif

			break;
#endif

#ifdef SYSTEM_TOOLKIT_ENABLE_TIMER_2
		case 2:
			resolution = 8;
			context = &INTERRUPT_CONTEXT_2;

#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
			powerReductionRegister = &PRR0;
#else
			powerReductionRegister = NULL;
#endif
			powerReductionEnableTimerBit = PRTIM2;

			controlRegisterA = &TCCR2A;
			controlRegisterB = &TCCR2B;
			controlRegisterC = NULL;

			timerCounterLow = &TCNT2;
			timerCounterHigh = NULL;

			interruptMask = &TIMSK2;

			inputCaptureInterruptEnableBit = 0;
			outputCompareAInterruptEnableBit = OCIE2A;
			outputCompareBInterruptEnableBit = OCIE2B;
			outputCompareCInterruptEnableBit = 0;
			overflowInterruptEnableBit = TOIE2;

			interruptFlag = &TIFR2;

			inputCaptureInterruptFlag = 0;
			outputCompareAInterruptFlag = OCF2A;
			outputCompareBInterruptFlag = OCF2B;
			outputCompareCInterruptFlag = 0;
			overflowInterruptFlag = TOV2;

			clockSelectBit0 = CS20;
			clockSelectBit1 = CS21;
			clockSelectBit2 = CS22;

			waveGeneratorModeBit0 = WGM20;
			waveGeneratorModeBit1 = WGM21;
			waveGeneratorModeBit2 = WGM22;
			waveGeneratorModeBit3 = 0;

			outputCompareAModeBit0 = COM2A0;
			outputCompareAModeBit1 = COM2A1;

			outputCompareBModeBit0 = COM2B0;
			outputCompareBModeBit1 = COM2B1;

			outputCompareCModeBit0 = 0;
			outputCompareCModeBit1 = 0;

			break;
#endif

#ifdef SYSTEM_TOOLKIT_ENABLE_TIMER_3
		case 3:
			resolution = 16;
			context = &INTERRUPT_CONTEXT_3;

			powerReductionRegister = &PRR1;
			powerReductionEnableTimerBit = PRTIM3;

			controlRegisterA = &TCCR3A;
			controlRegisterB = &TCCR3B;
			controlRegisterC = &TCCR3C;

			timerCounterLow = &TCNT3L;
			timerCounterHigh = &TCNT3H;

			interruptMask = &TIMSK3;
			inputCaptureInterruptEnableBit = ICIE3;
			outputCompareAInterruptEnableBit = OCIE3A;
			outputCompareBInterruptEnableBit = OCIE3B;
			outputCompareCInterruptEnableBit = OCIE3C;
			overflowInterruptEnableBit = TOIE3;

			interruptFlag = &TIFR3;
			inputCaptureInterruptFlag = ICF3;
			outputCompareAInterruptFlag = OCF3A;
			outputCompareBInterruptFlag = OCF3B;
			outputCompareCInterruptFlag = OCF3C;
			overflowInterruptFlag = TOV3;

			clockSelectBit0 = CS30;
			clockSelectBit1 = CS31;
			clockSelectBit2 = CS32;

			waveGeneratorModeBit0 = WGM30;
			waveGeneratorModeBit1 = WGM31;
			waveGeneratorModeBit2 = WGM32;
			waveGeneratorModeBit3 = WGM33;

			outputCompareAModeBit0 = COM3A0;
			outputCompareAModeBit1 = COM3A1;

			outputCompareBModeBit0 = COM3B0;
			outputCompareBModeBit1 = COM3B1;

			outputCompareCModeBit0 = COM3C0;
			outputCompareCModeBit1 = COM3C1;

			break;
#endif

#ifdef SYSTEM_TOOLKIT_ENABLE_TIMER_4
		case 4:
			resolution = 16;
			context = &INTERRUPT_CONTEXT_4;

			powerReductionRegister = &PRR1;
			powerReductionEnableTimerBit = PRTIM4;

			controlRegisterA = &TCCR4A;
			controlRegisterB = &TCCR4B;
			controlRegisterC = &TCCR4C;

			timerCounterLow = &TCNT4L;
			timerCounterHigh = &TCNT4H;

			interruptMask = &TIMSK4;
			inputCaptureInterruptEnableBit = ICIE4;
			outputCompareAInterruptEnableBit = OCIE4A;
			outputCompareBInterruptEnableBit = OCIE4B;
			outputCompareCInterruptEnableBit = OCIE4C;
			overflowInterruptEnableBit = TOIE4;

			interruptFlag = &TIFR4;
			inputCaptureInterruptFlag = ICF4;
			outputCompareAInterruptFlag = OCF4A;
			outputCompareBInterruptFlag = OCF4B;
			outputCompareCInterruptFlag = OCF4C;
			overflowInterruptFlag = TOV4;

			clockSelectBit0 = CS40;
			clockSelectBit1 = CS41;
			clockSelectBit2 = CS42;

			waveGeneratorModeBit0 = WGM40;
			waveGeneratorModeBit1 = WGM41;
			waveGeneratorModeBit2 = WGM42;
			waveGeneratorModeBit3 = WGM43;

			outputCompareAModeBit0 = COM4A0;
			outputCompareAModeBit1 = COM4A1;

			outputCompareBModeBit0 = COM4B0;
			outputCompareBModeBit1 = COM4B1;

			outputCompareCModeBit0 = COM4C0;
			outputCompareCModeBit1 = COM4C1;

			break;
#endif

#ifdef SYSTEM_TOOLKIT_ENABLE_TIMER_5
		case 5:
			resolution = 16;
			context = &INTERRUPT_CONTEXT_5;

			powerReductionRegister = &PRR1;
			powerReductionEnableTimerBit = PRTIM5;

			controlRegisterA = &TCCR5A;
			controlRegisterB = &TCCR5B;
			controlRegisterC = &TCCR5C;

			timerCounterLow = &TCNT5L;
			timerCounterHigh = &TCNT5H;

			interruptMask = &TIMSK5;
			inputCaptureInterruptEnableBit = ICIE5;
			outputCompareAInterruptEnableBit = OCIE5A;
			outputCompareBInterruptEnableBit = OCIE5B;
			outputCompareCInterruptEnableBit = OCIE5C;
			overflowInterruptEnableBit = TOIE5;

			interruptFlag = &TIFR5;
			inputCaptureInterruptFlag = ICF5;
			outputCompareAInterruptFlag = OCF5A;
			outputCompareBInterruptFlag = OCF5B;
			outputCompareCInterruptFlag = OCF5C;
			overflowInterruptFlag = TOV5;

			clockSelectBit0 = CS50;
			clockSelectBit1 = CS51;
			clockSelectBit2 = CS52;

			waveGeneratorModeBit0 = WGM50;
			waveGeneratorModeBit1 = WGM51;
			waveGeneratorModeBit2 = WGM52;
			waveGeneratorModeBit3 = WGM53;

			outputCompareAModeBit0 = COM5A0;
			outputCompareAModeBit1 = COM5A1;

			outputCompareBModeBit0 = COM5B0;
			outputCompareBModeBit1 = COM5B1;

			outputCompareCModeBit0 = COM5C0;
			outputCompareCModeBit1 = COM5C1;

			break;
#endif

	}
}

//------------------------------------------------------------------------------

Timer::~Timer()
{
	stop();
}

//==============================================================================
//
// Public operations
//
//==============================================================================

void Timer::setEventHandler(TimerEventHandler *aHandler)
{
	context->interruptHandler = aHandler;
}

//------------------------------------------------------------------------------

void Timer::start()
{
	if (started)
		return;

	if (powerReductionRegister)
	{
		bitClear(*powerReductionRegister, powerReductionEnableTimerBit);
	}

	started = true;
}

//------------------------------------------------------------------------------

void Timer::stop()
{
	if (!started)
		return;

	if (powerReductionRegister)
	{
		bitSet(*powerReductionRegister, powerReductionEnableTimerBit);
	}

	started = false;
}

//==============================================================================
//
// Internal operations
//
//==============================================================================

#ifdef SYSTEM_TOOLKIT_ENABLE_TIMER_0
ISR(TIMER0_COMPA_vect)
{
	INTERRUPT_CONTEXT_0.interruptHandler->onTimerOutputMatchA();
}

ISR(TIMER0_COMPB_vect)
{
	INTERRUPT_CONTEXT_0.interruptHandler->onTimerOutputMatchB();
}
#endif

//------------------------------------------------------------------------------

#ifdef SYSTEM_TOOLKIT_ENABLE_TIMER_1
ISR(TIMER1_OVF_vect)
{
	INTERRUPT_CONTEXT_1.interruptHandler->onTimerOverflow();
}

ISR(TIMER1_COMPA_vect)
{
	INTERRUPT_CONTEXT_1.interruptHandler->onTimerOutputMatchA();
}

ISR(TIMER1_COMPB_vect)
{
	INTERRUPT_CONTEXT_1.interruptHandler->onTimerOutputMatchB();
}

#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
ISR(TIMER1_COMPC_vect)
{
	INTERRUPT_CONTEXT_1.interruptHandler->onTimerOutputMatchC();
}
#endif

ISR(TIMER1_CAPT_vect)
{
	INTERRUPT_CONTEXT_1.interruptHandler->onTimerInputCapture();
}
#endif

//------------------------------------------------------------------------------

#ifdef SYSTEM_TOOLKIT_ENABLE_TIMER_2
ISR(TIMER2_OVF_vect)
{
	INTERRUPT_CONTEXT_2.interruptHandler->onTimerOverflow();
}

ISR(TIMER2_COMPA_vect)
{
	INTERRUPT_CONTEXT_2.interruptHandler->onTimerOutputMatchA();
}

ISR(TIMER2_COMPB_vect)
{
	INTERRUPT_CONTEXT_2.interruptHandler->onTimerOutputMatchB();
}
#endif

//------------------------------------------------------------------------------

#ifdef SYSTEM_TOOLKIT_ENABLE_TIMER_3
ISR(TIMER3_OVF_vect)
{
	INTERRUPT_CONTEXT_3.interruptHandler->onTimerOverflow();
}

ISR(TIMER3_COMPA_vect)
{
	INTERRUPT_CONTEXT_3.interruptHandler->onTimerOutputMatchA();
}

ISR(TIMER3_COMPB_vect)
{
	INTERRUPT_CONTEXT_3.interruptHandler->onTimerOutputMatchB();
}

ISR(TIMER3_COMPC_vect)
{
	INTERRUPT_CONTEXT_3.interruptHandler->onTimerOutputMatchC();
}

ISR(TIMER3_CAPT_vect)
{
	INTERRUPT_CONTEXT_3.interruptHandler->onTimerInputCapture();
}
#endif

//------------------------------------------------------------------------------

#ifdef SYSTEM_TOOLKIT_ENABLE_TIMER_4
ISR(TIMER4_OVF_vect)
{
	INTERRUPT_CONTEXT_4.interruptHandler->onTimerOverflow();
}

ISR(TIMER4_COMPA_vect)
{
	INTERRUPT_CONTEXT_4.interruptHandler->onTimerOutputMatchA();
}

ISR(TIMER4_COMPB_vect)
{
	INTERRUPT_CONTEXT_4.interruptHandler->onTimerOutputMatchB();
}

ISR(TIMER4_COMPC_vect)
{
	INTERRUPT_CONTEXT_4.interruptHandler->onTimerOutputMatchC();
}

ISR(TIMER4_CAPT_vect)
{
	INTERRUPT_CONTEXT_4.interruptHandler->onTimerInputCapture();
}
#endif

//------------------------------------------------------------------------------

#ifdef SYSTEM_TOOLKIT_ENABLE_TIMER_5
ISR(TIMER5_OVF_vect)
{
	INTERRUPT_CONTEXT_5.interruptHandler->onTimerOverflow();
}

ISR(TIMER5_COMPA_vect)
{
	INTERRUPT_CONTEXT_5.interruptHandler->onTimerOutputMatchA();
}

ISR(TIMER5_COMPB_vect)
{
	INTERRUPT_CONTEXT_5.interruptHandler->onTimerOutputMatchB();
}

ISR(TIMER5_COMPC_vect)
{
	INTERRUPT_CONTEXT_5.interruptHandler->onTimerOutputMatchC();
}

ISR(TIMER5_CAPT_vect)
{
	INTERRUPT_CONTEXT_5.interruptHandler->onTimerInputCapture();
}
#endif

//------------------------------------------------------------------------------
