/*
 * DimmingMultiplexer.h
 *
 *  Created on: 18 mars 2021
 *      Author: Sébastien GRENIER
 */

#ifndef DIMMINGMULTIPLEXER_H_
#define DIMMINGMULTIPLEXER_H_

#include "Arduino.h"
#include "Timer.h"

class DimmingMultiplexer: public TimerEventHandler
{
	private:

	static DimmingMultiplexer *GLOBAL_INSTANCE;

	public:

	static DimmingMultiplexer* getInstance()
	{
		return GLOBAL_INSTANCE;
	}

	static DimmingMultiplexer* initialize(uint8_t aTimerNumber,
			uint8_t aChannelCount);

	private:

	Timer *timer;

	volatile uint8_t tick;

	volatile uint16_t clockTick;
	volatile uint32_t tenMilliseconds;

	bool suspended;

	uint8_t channelCount;
	uint8_t *levels;
	uint8_t *bits;
	bool *reversed;
	volatile uint8_t **ports;

	DimmingMultiplexer(uint8_t aTimerNumber, uint8_t aChannelCount);

	public:

	virtual ~DimmingMultiplexer();

	uint32_t getTenMilliseconds()
	{
		return tenMilliseconds;
	}

	void start();
	void stop();

	void onTimerOutputMatchA();

	void suspend();
	void resume();

	void registerChannel(uint8_t index, uint8_t pinNumber, boolean reversed);

	void setChannelLevel(uint8_t index, uint8_t dimmingLevel0To20);
	void setChannelOn(uint8_t index);
	void setChannelOff(uint8_t index);
};

#endif /* DIMMINGMULTIPLEXER_H_ */
