/*
 * Max471CurrentSensor.h
 *
 *  Created on: 3 mars 2021
 *      Author: Sébastien GRENIER
 */

#ifndef Max471CurrentSensor_H_
#define Max471CurrentSensor_H_

#include "Arduino.h"
#include "CurrentSensor.h"

class Max471CurrentSensor: public CurrentSensor
{
	private:

	long vccMilli;

	public:

	Max471CurrentSensor(uint8_t anAnalogPinNumber, uint16_t aVccMilli);

	bool convertSample(uint16_t sample, uint16_t* output);
};

#endif /* Max471CurrentSensor_H_ */
