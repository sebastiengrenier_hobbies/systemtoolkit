/*
 * CurrentSensor.h
 *
 *  Created on: 3 mars 2021
 *      Author: Sébastien GRENIER
 */

#ifndef CURRENTSENSOR_H_
#define CURRENTSENSOR_H_

#include "Arduino.h"

class CurrentSensor
{
	private:

	uint8_t analogPinNumber;

	boolean started;

	public:

	CurrentSensor(uint8_t anAnalogPinNumber);
	virtual ~CurrentSensor();

	virtual bool convertSample(uint16_t sample, uint16_t* output) = 0;

	//
	// Public operations
	//

	void start();
	void stop();

	uint16_t senseAverageSample(uint16_t sampleCount);
	uint16_t senseDurationSample(uint16_t milliseconds);
	uint16_t sensePeakDuration(uint16_t milliseconds, uint16_t peakCurrent);
};

#endif /* CURRENTSENSOR_H_ */
