#ifndef __SYSTEM_TOOLKIT_INCLUDED__
#define __SYSTEM_TOOLKIT_INCLUDED__

#include "DimmingMultiplexer.h"

#include "Timer.h"
#include "TimerEventHandler.h"

#include "CurrentSensor.h"
#include "ACS712CurrentSensor.h"
#include "Max471CurrentSensor.h"
#include "LM358CurrentSensor.h"

#endif
