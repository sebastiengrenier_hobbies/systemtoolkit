/*
 * Timer.h
 *
 *  Created on: 5 mars 2021
 *      Author: sgrenier
 */

#ifndef Timer_H_
#define Timer_H_

#include "Arduino.h"
#include "TimerEventHandler.h"

#include "SystemToolkitConfig.h"

//==============================================================================
//
// Internal structure used to callback object from interrupt handlers
//
//==============================================================================

typedef struct
{
	TimerEventHandler *interruptHandler;
} INTERRUPT_CONTEXT;

//==============================================================================
//
// Public enums used to setup timer
//
//==============================================================================

typedef enum
{
	TIMER_8BIT_MODE_NORMAL = 0b00000000,
	TIMER_8BIT_MODE_PHASE_CORRECT_PWM = 0b00000001,
	TIMER_8BIT_MODE_CLEAR_TIMER_ON_MATCH_OCR = 0b00000010,
	TIMER_8BIT_MODE_FAST_PWM = 0b00000011,
	TIMER_8BIT_MODE_RESERVED1 = 0b00000100,
	TIMER_8BIT_MODE_PHASE_PWM_OCR = 0b00000101,
	TIMER_8BIT_MODE_RESERVED2 = 0b00000110,
	TIMER_8BIT_MODE_FAST_PWM_OCR = 0b00000111,
	TIMER_16BIT_MODE_NORMAL = 0b10000,
	TIMER_16BIT_MODE_PHASE_CORRECT_PWM_8_BITS = 0b10000001,
	TIMER_16BIT_MODE_PHASE_CORRECT_PWM_9_BITS = 0b10000010,
	TIMER_16BIT_MODE_PHASE_CORRECT_PWM_10_BITS = 0b10000011,
	TIMER_16BIT_MODE_CLEAR_TIMER_ON_MATCH_OCR = 0b10000100,
	TIMER_16BIT_MODE_FAST_PWM_8_BITS = 0b10000101,
	TIMER_16BIT_MODE_FAST_PWM_9_BITS = 0b10000110,
	TIMER_16BIT_MODE_FAST_PWM_10_BITS = 0b10000111,
	TIMER_16BIT_MODE_PHASE_AND_FREQUENCY_CORRECT_PWM_TOP_ICR = 0b10001000,
	TIMER_16BIT_MODE_PHASE_AND_FREQUENCY_CORRECT_PWM_TOP_OCR = 0b10001001,
	TIMER_16BIT_MODE_PHASE_CORRECT_PWM_TOP_ICR = 0b10001010,
	TIMER_16BIT_MODE_PHASE_CORRECT_PWM_TOP_OCR = 0b10001011,
	TIMER_16BIT_MODE_CLEAR_TIMER_ON_MATCH_ICR = 0b10001100,
	TIMER_16BIT_MODE_RESERVED = 0b10001101,
	TIMER_16BIT_MODE_FAST_PWM_ICR = 0b10001110,
	TIMER_16BIT_MODE_FAST_PWM_OCR = 0b10001111
} TIMER_MODE;

//------------------------------------------------------------------------------

typedef enum
{
	CLOCK_MODE_NONE = 0b000,
	CLOCK_MODE_INTERNAL_PRESCALE_1 = 0b001,
	CLOCK_MODE_INTERNAL_PRESCALE_8 = 0b010,
	CLOCK_MODE_INTERNAL_PRESCALE_64 = 0b011,
	CLOCK_MODE_INTERNAL_PRESCALE_256 = 0b100,
	CLOCK_MODE_INTERNAL_PRESCALE_1024 = 0b101,
	CLOCK_MODE_EXTERNAL_FALLING_EDGE = 0b110,
	CLOCK_MODE_EXTERNAL_RISING_EDGE = 0b111,
	CLOCK_MODE_TIMER2_NONE = 0b10000000,
	CLOCK_MODE_TIMER2_INTERNAL_PRESCALE_1 = 0b10000001,
	CLOCK_MODE_TIMER2_INTERNAL_PRESCALE_8 = 0b10000010,
	CLOCK_MODE_TIMER2_INTERNAL_PRESCALE_32 = 0b10000011,
	CLOCK_MODE_TIMER2_INTERNAL_PRESCALE_64 = 0b10000100,
	CLOCK_MODE_TIMER2_INTERNAL_PRESCALE_128 = 0b10000101,
	CLOCK_MODE_TIMER2_INTERNAL_PRESCALE_256 = 0b10000110,
	CLOCK_MODE_TIMER2_INTERNAL_PRESCALE_1024 = 0b10000111
} CLOCK_MODE;

//------------------------------------------------------------------------------

typedef enum
{
	COMPARE_MODE_NONE = 0b00,
	COMPARE_MODE_TOGGLE_ON_MATCH = 0b01,
	COMPARE_MODE_CLEAR_ON_MATCH = 0b10,
	COMPARE_MODE_SET_ON_MATCH = 0b11
} OUTPUT_COMPARE_MODE;

//------------------------------------------------------------------------------

class Timer
{
	protected:

	uint8_t timerNumber;
	uint8_t resolution;

	INTERRUPT_CONTEXT *context;

	volatile uint8_t *powerReductionRegister;
	uint8_t powerReductionEnableTimerBit;

	volatile uint8_t *controlRegisterA;
	volatile uint8_t *controlRegisterB;
	volatile uint8_t *controlRegisterC;

	volatile uint8_t *timerCounterLow;
	volatile uint8_t *timerCounterHigh;

	volatile uint8_t *interruptMask;

	uint8_t inputCaptureInterruptEnableBit;
	uint8_t outputCompareAInterruptEnableBit;
	uint8_t outputCompareBInterruptEnableBit;
	uint8_t outputCompareCInterruptEnableBit;
	uint8_t overflowInterruptEnableBit;

	volatile uint8_t *interruptFlag;

	uint8_t inputCaptureInterruptFlag;
	uint8_t outputCompareAInterruptFlag;
	uint8_t outputCompareBInterruptFlag;
	uint8_t outputCompareCInterruptFlag;
	uint8_t overflowInterruptFlag;

	uint8_t clockSelectBit0;
	uint8_t clockSelectBit1;
	uint8_t clockSelectBit2;

	uint8_t waveGeneratorModeBit0;
	uint8_t waveGeneratorModeBit1;
	uint8_t waveGeneratorModeBit2;
	uint8_t waveGeneratorModeBit3;

	uint8_t outputCompareAModeBit0;
	uint8_t outputCompareAModeBit1;

	uint8_t outputCompareBModeBit0;
	uint8_t outputCompareBModeBit1;

	uint8_t outputCompareCModeBit0;
	uint8_t outputCompareCModeBit1;

	bool started;

	protected:

	Timer(uint8_t aTimerNumber);

	public:

	virtual ~Timer();

	static Timer* getInstance(uint8_t timerNumber);

	uint8_t getNumber()
	{
		return timerNumber;
	}

	uint8_t getResolution()
	{
		return resolution;
	}

	void setEventHandler(TimerEventHandler *aHandler);

	virtual void enableInputCaptureInterrupt(__attribute__((unused))bool value)
	{
	}

	virtual void enableOutputCompareAInterrupt(
			__attribute__((unused))bool value)
	{
	}

	virtual void enableOutputCompareBInterrupt(
			__attribute__((unused))bool value)
	{
	}

	virtual void enableOutputCompareCInterrupt(
			__attribute__((unused))bool value)
	{
	}

	virtual void enableOverflowInterrupt(__attribute__((unused))bool value)
	{
	}

	virtual void setOutputCompareAMode(__attribute__((unused))OUTPUT_COMPARE_MODE value)
	{
	}

	virtual void setOutputCompareA(__attribute__((unused))uint16_t value)
	{
	}

	virtual void setOutputCompareBMode(__attribute__((unused))OUTPUT_COMPARE_MODE value)
	{
	}

	virtual void setOutputCompareB(__attribute__((unused))uint16_t value)
	{
	}

	virtual void setOutputCompareCMode(__attribute__((unused))OUTPUT_COMPARE_MODE value)
	{
	}

	virtual void setOutputCompareC(__attribute__((unused))uint16_t value)
	{
	}

	virtual void setInputCapture(__attribute__((unused))uint16_t value)
	{
	}

	virtual void setClockMode(__attribute__((unused))CLOCK_MODE value)
	{
	}

	virtual void setTimerMode(__attribute__((unused))TIMER_MODE value)
	{
	}

	void start();

	void stop();
};

#endif /* Timer_H_ */
