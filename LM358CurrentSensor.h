/*
 * LM358CurrentSensor.h
 *
 *  Created on: 27 mars 2021
 *      Author: Sébastien GRENIER
 */

#ifndef SYSTEMTOOLKIT_LM358CURRENTSENSOR_H_
#define SYSTEMTOOLKIT_LM358CURRENTSENSOR_H_

#include "Arduino.h"
#include "CurrentSensor.h"

class LM358CurrentSensor: public CurrentSensor
{
	private:

	long vccMilli;

	public:

	LM358CurrentSensor(uint8_t anAnalogPinNumber, uint16_t aVccMilli);

	bool convertSample(uint16_t sample, uint16_t *output);
};

#endif /* SYSTEMTOOLKIT_LM358CURRENTSENSOR_H_ */
