/*
 * Timer16Bit.cpp
 *
 *  Created on: 8 mars 2021
 *      Author: Sébastien GRENIER
 */

#include "Timer16Bit.h"

//==============================================================================
//
// Construction / destruction
//
//==============================================================================

Timer16Bit::Timer16Bit(uint8_t aTimerNumber) :
		Timer(aTimerNumber)
{
	switch (aTimerNumber)
	{

#ifdef SYSTEM_TOOLKIT_ENABLE_TIMER_1
		case 1:

			outputCompareA = &OCR1A;
			outputCompareB = &OCR1B;
#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
			outputCompareC = &OCR1C;
#else
			outputCompareC = NULL;
#endif
			inputCapture = &ICR1;

			break;
#endif

#ifdef SYSTEM_TOOLKIT_ENABLE_TIMER_3
		case 3:

			outputCompareA = &OCR3A;
			outputCompareB = &OCR3B;
			outputCompareC = &OCR3C;
			inputCapture = &ICR3;

			break;
#endif

#ifdef SYSTEM_TOOLKIT_ENABLE_TIMER_4
		case 4:

			outputCompareA = &OCR4A;
			outputCompareB = &OCR4B;
			outputCompareC = &OCR4C;
			inputCapture = &ICR4;

			break;
#endif

#ifdef SYSTEM_TOOLKIT_ENABLE_TIMER_5
		case 5:

			outputCompareA = &OCR5A;
			outputCompareB = &OCR5B;
			outputCompareC = &OCR5C;
			inputCapture = &ICR5;

			break;
#endif
	}
}

//------------------------------------------------------------------------------

Timer16Bit::~Timer16Bit()
{
}

//==============================================================================
//
// Public operations
//
//==============================================================================

void Timer16Bit::enableInputCaptureInterrupt(bool value)
{
	if (value)
		bitSet(*interruptMask, inputCaptureInterruptEnableBit);
	else
		bitClear(*interruptMask, inputCaptureInterruptEnableBit);
}

//------------------------------------------------------------------------------

void Timer16Bit::enableOutputCompareAInterrupt(bool value)
{
	if (value)
		bitSet(*interruptMask, outputCompareAInterruptEnableBit);
	else
		bitClear(*interruptMask, outputCompareAInterruptEnableBit);
}

//------------------------------------------------------------------------------

void Timer16Bit::enableOutputCompareBInterrupt(bool value)
{
	if (value)
		bitSet(*interruptMask, outputCompareBInterruptEnableBit);
	else
		bitClear(*interruptMask, outputCompareBInterruptEnableBit);
}

//------------------------------------------------------------------------------

void Timer16Bit::enableOutputCompareCInterrupt(
		__attribute__((unused))bool value)
{
#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
	if (value)
	bitSet(*interruptMask, outputCompareCInterruptEnableBit);
	else
			bitClear(*interruptMask, outputCompareCInterruptEnableBit);
#endif
}

//------------------------------------------------------------------------------

void Timer16Bit::enableOverflowInterrupt(bool value)
{
	if (value)
		bitSet(*interruptMask, overflowInterruptEnableBit);
	else
		bitClear(*interruptMask, overflowInterruptEnableBit);
}

//------------------------------------------------------------------------------

void Timer16Bit::setOutputCompareAMode(OUTPUT_COMPARE_MODE value)
{
	bitWrite(*controlRegisterA, outputCompareAModeBit0, value & 0x01);
	bitWrite(*controlRegisterA, outputCompareAModeBit1, value & 0x02);
}

//------------------------------------------------------------------------------

void Timer16Bit::setOutputCompareA(uint16_t value)
{
	*outputCompareA = value;
}

//------------------------------------------------------------------------------

void Timer16Bit::setOutputCompareBMode(OUTPUT_COMPARE_MODE value)
{
	bitWrite(*controlRegisterA, outputCompareBModeBit0, value & 0x01);
	bitWrite(*controlRegisterA, outputCompareBModeBit1, value & 0x02);
}

//------------------------------------------------------------------------------

void Timer16Bit::setOutputCompareB(uint16_t value)
{
	*outputCompareB = value;
}

//------------------------------------------------------------------------------

void Timer16Bit::setOutputCompareCMode(__attribute__((unused))OUTPUT_COMPARE_MODE value)
{
#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
	bitWrite(*controlRegisterA, outputCompareCModeBit0, value & 0x01);
	bitWrite(*controlRegisterA, outputCompareCModeBit1, value & 0x02);
#endif
}

//------------------------------------------------------------------------------

void Timer16Bit::setOutputCompareC(__attribute__((unused))uint16_t value)
{
#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
	*outputCompareC = value;
#endif
}

//------------------------------------------------------------------------------

void Timer16Bit::setInputCapture(uint16_t value)
{
	*inputCapture = value;
}

//------------------------------------------------------------------------------

void Timer16Bit::setClockMode(CLOCK_MODE clockMode)
{
	bitWrite(*controlRegisterB, clockSelectBit0, clockMode & 0x01);
	bitWrite(*controlRegisterB, clockSelectBit1, clockMode & 0x02);
	bitWrite(*controlRegisterB, clockSelectBit2, clockMode & 0x04);
}

//------------------------------------------------------------------------------

void Timer16Bit::setTimerMode(TIMER_MODE operationMode)
{
	bitWrite(*controlRegisterA, waveGeneratorModeBit0, operationMode & 0x01);
	bitWrite(*controlRegisterA, waveGeneratorModeBit1, operationMode & 0x02);
	bitWrite(*controlRegisterB, waveGeneratorModeBit2, operationMode & 0x04);
	bitWrite(*controlRegisterB, waveGeneratorModeBit3, operationMode & 0x08);
}

//------------------------------------------------------------------------------
