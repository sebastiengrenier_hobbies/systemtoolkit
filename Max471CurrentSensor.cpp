/*
 * Max471CurrentSensor.cpp
 *
 *  Created on: 3 mars 2021
 *      Author: Sébastien GRENIER
 */

#include "Max471CurrentSensor.h"

Max471CurrentSensor::Max471CurrentSensor(uint8_t anAnalogPinNumber,
		uint16_t aVccMilli) :
		CurrentSensor(anAnalogPinNumber)
{
	vccMilli = aVccMilli;
}

bool Max471CurrentSensor::convertSample(uint16_t sample, uint16_t *output)
{
	if ((sample == 0) || (sample > 1023))
		return false;

	uint32_t value = (uint32_t)sample;
	value *= vccMilli;
	value /= (uint32_t)1023;

	*output = (uint16_t) value;

	return true;
}

