/*
 * SystemToolkitConfig.h
 *
 *  Created on: 19 févr. 2021
 *      Author: Sébastien GRENIER
 *
 * Use this configuration file to setup the SystemToolkit library.
 *
 * You may also use one of the configuration sample provided in the sampleconfig
 * directory.
 *
 */

#ifndef SYSTEMTOOLKITCONFIG_H_
#define SYSTEMTOOLKITCONFIG_H_

#define SYSTEM_TOOLKIT_ENABLE_TIMER_0
#define SYSTEM_TOOLKIT_ENABLE_TIMER_1
#define SYSTEM_TOOLKIT_ENABLE_TIMER_2

#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)

#define SYSTEM_TOOLKIT_ENABLE_TIMER_3
#define SYSTEM_TOOLKIT_ENABLE_TIMER_4
#define SYSTEM_TOOLKIT_ENABLE_TIMER_5

#endif

#endif /* SYSTEMTOOLKITCONFIG_H_ */
