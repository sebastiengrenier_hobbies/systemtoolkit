/*
 * ACS712CurrentSensor.h
 *
 *  Created on: 3 mars 2021
 *      Author: Sébastien GRENIER
 */

#ifndef ACS712CURRENTSENSOR_H_
#define ACS712CURRENTSENSOR_H_

#include "Arduino.h"

#include "CurrentSensor.h"

class ACS712CurrentSensor: public CurrentSensor
{
	private:

	uint16_t vccMilli;
	uint16_t milliVoltsPerAmps;

	public:

	ACS712CurrentSensor(uint8_t anAnalogPinNumber, uint16_t aVccMilli = 5000,
			uint16_t aMilliVoltsPerAmps = 100);

	bool convertSample(uint16_t sample, uint16_t *output);
};

#endif /* ACS712CURRENTSENSOR_H_ */
