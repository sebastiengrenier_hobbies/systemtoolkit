/*
 * Timer16Bit.h
 *
 *  Created on: 8 mars 2021
 *      Author: Sébastien GRENIER
 */

#ifndef Timer16Bit_H_
#define Timer16Bit_H_

#include "Timer.h"

class Timer16Bit: public Timer
{
	private:

	volatile uint16_t *outputCompareA;
	volatile uint16_t *outputCompareB;
	volatile uint16_t *outputCompareC;
	volatile uint16_t *inputCapture;

	public:

	Timer16Bit(uint8_t aTimerNumber);
	virtual ~Timer16Bit();

	void enableInputCaptureInterrupt(bool value);

	void enableOutputCompareAInterrupt(bool value);

	void enableOutputCompareBInterrupt(bool value);

	void enableOutputCompareCInterrupt(bool value);

	void enableOverflowInterrupt(bool value);

	void setOutputCompareAMode(OUTPUT_COMPARE_MODE value);

	void setOutputCompareA(uint16_t value);

	void setOutputCompareBMode(OUTPUT_COMPARE_MODE value);

	void setOutputCompareB(uint16_t value);

	void setOutputCompareCMode(OUTPUT_COMPARE_MODE value);

	void setOutputCompareC(uint16_t value);

	void setInputCapture(uint16_t value);

	void setClockMode(CLOCK_MODE value);

	void setTimerMode(TIMER_MODE value);
};

#endif /* Timer16Bit_H_ */
