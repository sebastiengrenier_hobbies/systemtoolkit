/*
 * Timer8Bit.h
 *
 *  Created on: 8 mars 2021
 *      Author: Sébastien GRENIER
 */

#ifndef TIMER8BIT_H_
#define TIMER8BIT_H_

#include "Timer.h"

class Timer8Bit: public Timer
{
	private:

	volatile uint8_t *outputCompareA;
	volatile uint8_t *outputCompareB;

	public:
	Timer8Bit(uint8_t aTimerNumber);
	virtual ~Timer8Bit();

	void enableOutputCompareAInterrupt(bool value);

	void enableOutputCompareBInterrupt(bool value);

	void enableOverflowInterrupt(bool value);

	void setOutputCompareAMode(OUTPUT_COMPARE_MODE value);

	void setOutputCompareA(uint16_t value);

	void setOutputCompareBMode(OUTPUT_COMPARE_MODE value);

	void setOutputCompareB(uint16_t value);

	void setClockMode(CLOCK_MODE value);

	void setTimerMode(TIMER_MODE value);
};

#endif /* TIMER8BIT_H_ */
